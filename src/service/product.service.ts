import { Request, Response } from "express";
import ProductModel, {ProductInput} from "../models/product.model";

export async function createProduct(req: Request, res: Response) {
  const body: ProductInput = req.body;
 
  await ProductModel.create({ ...body })
    .then((resData: any) => {
      res.status(200).json(
        {
          message: "New Product was created successfully",
          createdProduct: resData
        })
    })
    .catch((err: any) => {
      res.status(500).json({
        error: err,
      });
    })
}


export async function getAllProduct(req: Request, res: Response) {

  await ProductModel.find()
    .then((resData) => {
      res.status(200).json({
        message: "List of Products",
        ListOfProducts: resData
      });
    })
    .catch((err) => {
      res.status(204).json({
        error: err
      });
    })
}

export async function getCategory(req: Request, res: Response) {
  const category  = req.params.category;
    
  await ProductModel.find({ category })

    .then((resData) => {
      if(resData) {
      res.status(200).json({
        message: "List of Products by Category",
        ListOfProducts: resData
      })
    } else
    {
      res.status(200).json({
        message: "Category Not Found"
      })
    }
    })
    .catch((err) => {
      res.status(204).json({
        error: err
      });
    })

}

export async function getProduct(req: Request, res: Response) {
  const productId  = req.params.productId;
    
  await ProductModel.findOne({ productId })

    .then((resData) => {
      if(resData) {
      res.status(200).json({
        message: "List of Products by Product ID",
        productByID: resData
      })
    } else
    {
      res.status(200).json({
        message: "ProductID Not Found"
      })
    }
    })
    .catch((err) => {
      res.status(204).json({
        error: err
      });
    })

}



export async function updateProduct(
  req: Request,
  res: Response
) {
  const productId = req.params.productId;
  const update = req.body;

 await ProductModel.updateOne({ productId }, update)
   .then((resData) => {
      res.status(200).json({
        message: "The Requested ID has been Updated",
        updatedByProduct: resData
    })

    })
    .catch((err) => {
      res.status(204).json({
        error: err
      });
    })
}




export async function deleteProduct(
  req: Request,
  res: Response
) {
  const productId = req.params.productId;

  await ProductModel.deleteOne({ productId })
  
    .then((resData) => {
    
     if(resData.deletedCount) {
      res.status(200).json({
        message: "The Requested Product ID has been Deleted",
        productDeleted: resData
        });
      }
      else { 
        res.json({
        message: "Record is not available for Delete"
      })}
    })
    .catch((err) => {
      res.status(204).json({
        error: err
      });
    })

}



