import { Express } from "express";
import {
  createProduct,
  getAllProduct,
  getProduct,
  updateProduct,
  deleteProduct,
  getCategory
} from "./service/product.service";



function routes(app: Express) {
  
  app.get("/api/products", getAllProduct);

  app.get("/api/products/:productId", getProduct);

  app.get("/api/products/category/:category", getCategory);

  app.post("/api/products", createProduct);

  app.put("/api/products/:productId", updateProduct);

  app.delete("/api/products/:productId", deleteProduct);

}

export default routes;
