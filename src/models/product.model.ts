import mongoose from "mongoose";
import { customAlphabet } from "nanoid";


const nanoid = customAlphabet("abcdefghijklmnopqrstuvwxyz0123456789", 10);

export interface ProductInput {
  title: string;
  description: string;
  price: number;
  image: string;
  mrp: number;
  off: string;
  category: string;
  review:number;
}

export interface ProductDocument extends ProductInput, mongoose.Document {
  createdAt: Date;
  updatedAt: Date;
}

const productSchema = new mongoose.Schema(
  {
    productId: {
      type: String,
      required: true,
      unique: true,
      default: () => `product_${nanoid()}`,
    },
    mrp: { type: Number, required: true},
    off: { type: String, required: true},
    title: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    image: { type: String, required: true },
    category: {type: String, required: true},
    review: { type: Number, required: true },
  },
  {
    timestamps: true,
  }
);

const ProductModel = mongoose.model<ProductDocument>("product_details", productSchema,"product_details");

export default ProductModel;
